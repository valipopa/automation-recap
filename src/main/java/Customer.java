/**
 * Created by vali on 03/08/2018.
 */
public class Customer {

    public Customer() {

    }

    private String name;

    public Customer(String name) {
        if (name == null || name.equals("linda")) {
            throw new IllegalArgumentException("Name is required");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getHelloWorld() {
        return "hello world";
    }
}