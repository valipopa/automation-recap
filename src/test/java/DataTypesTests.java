import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by vali on 02/08/2018.
 */
public class DataTypesTests {

    @Test
    public void test1() {
        String a = "wantsome";
        String b = new String(a);

        assertTrue(a == b); // fix the assert
        assertTrue(a.equals(b));
    }

    @Test
    public void test2() {
        String a = String.valueOf(2);   //integer to numeric string
        int i = Integer.parseInt(a); //numeric string to an int
        assertTrue(a.equals(i)); // fix the assert
        assertThat(String.format("Value of %d is", i), is("2"));
    }

    @Test
    public void test3() {
        String[][] countries = { { "United States", "New York" }, { "United Kingdom", "London" },
                { "Netherland", "Amsterdam" }, { "Japan", "Tokyo" }, { "France", "Paris" } };
        Map countryCapitals = ArrayUtils.toMap(countries);
        assertTrue(countryCapitals.get("Japan").toString().equals("Paris")
            && countryCapitals.get("Neterland").toString().equals("Tokyo"));
    }

    @Test
    public void test4() {
        assertTrue(hasLetters("Testare manuala"));
        assertTrue(hasLetters("Manual testing")); // fails, why?
        // add another assertion that is false, and one more that is true
    }

    @Test
    public void test5() {
        // Create an array that will pass the following assertion
        int[] myArray = new int[0];
        assertTrue(hasPairs(myArray));
    }

    public boolean hasLetters(String text) {
        int count = 0;
        for (int i = 0; i < text.length(); i++) {
            String subString = text.substring(i, i + 1);
            if (subString.equals("e")) {
                count++;
            }
        }

        return count == 2;
    }

    public boolean hasPairs(int[] numbers) {
        int count = 0;
        for (int i=0; i<(numbers.length-1); i++) {
            if (numbers[i]==13 && numbers[i+1]==13) {
                return true;
            }
        }

        return false;
    }

}