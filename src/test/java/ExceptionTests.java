import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by vali on 02/08/2018.
 */
public class ExceptionTests {

    @Test
    public void test1() {
        String text = null;
        assertTrue(text.contains("Wantsome"));
    }

    @Test
    public void test2() {
        String text = null;
        try {
            String newString = new String(text);
            assertThat(newString.length(), is(0));
        } catch (NullPointerException npe) {
        } finally {
            assertThat(text.charAt(7), is('\''));
        }
    }
}