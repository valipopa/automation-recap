import org.junit.Test;

/**
 * Created by vali on 02/08/2018.
 */
public class Exercises {

    /**
     * Print the first 100 numbers in Fibonacci series
     */
    @Test
    public void fibonacci1() {

    }

    /**
     * Print the numbers in Fibonacci series which are less than 100
     */
    @Test
    public void fibonacci2() {

    }

    /**
     * Remove duplicates chars in a String. E.g. "abcabcabcxyzzz" will become "abcxyz"
     */
    @Test
    public void removeDuplicatesInAString() {

    }

    /**
     * Create an array with integers then separate the values into two new arrays,
     *  one stores the even numbers and the other stores the odd numbers.
     */
    @Test
    public void splitArrays() {

    }

    /**
     * Reverse words of a sentence. E.g. "Wantsome is great" will become "great is Wantsome"
     */
    @Test
    public void removeWords() {

    }
}