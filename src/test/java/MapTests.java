import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by vali on 02/08/2018.
 */
public class MapTests {

    /**
     * Create a new HashMap, add some values in it.
     * Create a new Set, add all map keys into set.
     * Create a new List, add all map values into list.
     *
     * Assert that:
     *  - list size is equal to map size which is equal to set size, in the same assert
     *  - list contains all map values
     *  - set contains all map keys
     */
    @Test
    public void mapListSetTest() {

    }

}