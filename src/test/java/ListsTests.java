import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by vali on 02/08/2018.
 */
public class ListsTests {

    String name1 = "iPhone";
    String name2 = "Samsung";
    String sum;

    @Test
    public void test1() {
        Product product1 = new Product();
        Product product2 = new Product();

        List<Product> products = new ArrayList<Product>();

        product1.setName(name1);
        product1.setPrice(20.0);

        product2.setName(name2);
        product2.setPrice(15.0);

        products.add(product1);
        products.add(product2);

        assertThat(products.size(), is(3));
        assertThat(products.get(2), is(product2));
        assertTrue(products.get(1) == product1);

        String iPhone = new String("iPhone");
        if(products.get(0).getName() == iPhone) {
            System.out.println("Make sure this gets printed to the console");
        }

        for (Product product : products) {
            sum = sum + product.getPrice();
        }

        assertTrue(sum.equals(35));
    }

}