import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by vali on 02/08/2018.
 */
public class OtherTests {

    @Test
    public void test1() {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("a", 1);
        assertThat(map.size(), is(2));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void test2() {
        List<String> list = new ArrayList<String>();
        assertTrue(list.get(1) == "");
    }

    @Test
    public void test3() {
        // 'Want' word should be printed in the console when running this test
        switchCase(0); // why are two words printed?
    }

    @Test
    public void test4() {
        assertTrue(firstIsBigger(2, 1));
        // add an assertion that passes
    }

    @Test
    public void test5() {
        SimpleDateFormat format = new SimpleDateFormat( "dd.MM.yyyy" );
        Date date = null;
        try {
            date = format.parse("21.JAN.2018");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Date is: " + date);

        assertTrue(date.after(new Date()));
    }

    /**
     * Write a function that calculates the sum of the digits in a number
     */
    @Test
    public void test6() {

    }

    /**
     * Write a function that breaks a number (123456 e.g) into a sequence of individual digits (1 2 3 4 5 6)
     */
    @Test
    public void test7() {

    }

    @Test
    public void test8() {
        String a = "Wantsome";
        String b = "Wantsome";

        Assert.assertEquals(a, b, "String a is not equal to String b");
    }

    public boolean firstIsBigger(int a, int b) {
        if (a > b && (a - b) >= 2) {
            return true;
        }

        return false;
    }

    public static void switchCase(int index) {
        //int index = 0;
        switch (index) {
            case 0:
                System.out.println("Want");
            case 1:
                System.out.println("Some");
                break;
            case 2:
                System.out.println("Testing");
                break;
            default:
                System.out.println("Yes!");
        }
    }
}