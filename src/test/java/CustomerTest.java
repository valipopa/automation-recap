import org.junit.Test;

import java.util.IllegalFormatException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by vali on 03/08/2018.
 */
public class CustomerTest {

    Customer customer;

    @Test(expected = IllegalFormatException.class)
    public void exceptionTest() {
        String name = null;
        new Customer(name);
    }

    @Test
    public void validNameTest() {
        String expectedName = "linda";
        customer = new Customer(expectedName + " ");
        assertTrue(customer.getName().equals(expectedName)); // leave assertion unchanged, fix code
    }

    @Test
    public void getNameTest() {
        String expectedName = "linda";
        customer = new Customer(expectedName + " customer");
        String actualName = customer.getName();
        assertTrue(expectedName == actualName); // fix assertion
    }

    @Test
    public void helloWorldTest() {
        Customer customer = new Seller();
        assertEquals(customer.getHelloWorld(), "hello world"); // fix the code, not the assertion
    }
}