import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

/**
 * Created by vali on 02/08/2018.
 */
public class SetTests {

    @Test
    public void setTest() {
        Set<String> courses = new HashSet<>();

        courses.add("Manual Testing");
        courses.add("Automation testing");
        courses.add("Java");
        courses.add("Python");
        courses.add("Infrastructure");

        assertEquals(5, courses.size() - 1);

        assertTrue(courses.contains("Manual testing"));

        Iterator<String> iterator = courses.iterator();
        while(iterator.hasNext()) {
            assertTrue(iterator.next().isEmpty()); // change the assert in order to fix it
        }

        for(String course : courses) {
            assertThat(course, is(nullValue())); // change the assert in order to fix it
        }
    }

}